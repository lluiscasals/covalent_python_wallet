# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.

import os

from setuptools import setup, find_packages

from pkg_resources import get_distribution, DistributionNotFound


ROOT_PATH = os.path.dirname(os.path.realpath(__file__))
SOURCE_PATH = os.path.join(ROOT_PATH, 'source')
README_PATH = os.path.join(ROOT_PATH, 'README.rst')

try:
    release = get_distribution('covalent-python-wallet').version
    # take major/minor/patch
    VERSION = '.'.join(release.split('.')[:3])
except DistributionNotFound:
    # package is not installed
    VERSION = 'Unknown version'


version_template = '''
# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.

__version__ = {version!r}
'''

# Call main setup.
setup(
    name='covalent-python-wallet',
    description='Query viewer using covalent python API',
    author='Lluis Casals Marsol',
    author_email='ktmlleska@gmail.com',
    url='https://bitbucket.org/lluisBrokenC/covalent_python_wallet',
    long_description=open(README_PATH).read(),
    license='GNU General Public License v3.0 (GPLv3)',
    keywords='covalent, python, api, wallet',
    packages=find_packages(SOURCE_PATH),
    project_urls={
        "Documentation": "https://covalent-python-api.readthedocs.io/en/latest/",
        "Covalent API Documentation": "https://www.covalenthq.com/docs/api/",
        "Covalent Homepage": "https://www.covalenthq.com/",
    },
    package_dir={
        '': 'source'
    },
    use_scm_version={
        'write_to': 'source/covalent_wallet/_version.py',
        'write_to_template': version_template,
    },
    setup_requires=[
        'PySide2 >=5, <6',
    ],
    install_requires=[
        'covalent-python-api',
        'PySide2 >=5, <6',
    ],
    zip_safe=False,
    python_requires=">=3.7.6, <4.0"
)
