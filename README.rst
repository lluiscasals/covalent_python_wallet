
**********************
Covalent Python Wallet
**********************

Please see the video link at: https://youtu.be/-rVzklOWybM


Covalent Python Wallet is an example of a PySide UI connected to the covalent
python api for the Covalent OneMillionWallets - Avalanche Hackathon.

You can visually see all the queries included on the Covalent API, for all
supported chains including Avalanche mainnet and the Fuji testnet.

The project uses the Covalent Python API developed specifically for this
Hackathon. Please check the Covalent Python API site for more imformation about
the API at:
https://pypi.org/project/covalent-python-api/

Video of the Hackathon showing how it works at: https://youtu.be/-rVzklOWybM

***************
Requirements
***************

Python 3.7 should be installed on your system.

- It's recommended to use a virtual environment.

***************
Installation
***************

Create a python 3.7 virtual environment.

Clone the public repository::

        $ git clone git@bitbucket.org:lluisBrokenC/covalent_python_wallet.git

- Or download and extract the
  `zipball <https://bitbucket.org/lluisBrokenC/covalent_python_wallet/get/master.zip>`_

Add the repository to the PYTHONPATH environment variable:

- MacOs users::

        $ export PYTHONPATH="/<path_to_your_repository>/source/"

- Windows users:

    Add the following path to your PYTHONPATH environment variable:

    "c:\\<path_to_your_repository>\\source\\"

Install the requirements.txt:

- Go to your repository folder::

        $ cd <path_to_your_repository>
        $ pip install -r requirements.txt

Set up your Covalent API key to the 'COVALENT_API_KEY' environment variable:

- MacOs users::

        $ export COVALENT_API_KEY="<path_covalent_api_key>"

- Windows users:

    Add the covalent api key to your COVALENT_API_KEY environment variable.

Execute the wallet using::

        $ python source/covalent_python_wallet/main_wallet.py

************************
Copyright and license
************************

Copyright (c) 2021 Lluis Casals Marsol

Licensed under the GNU General Public License v3.0 (GPLv3); you may not use
this work except in compliance with the License. You may obtain a copy of the
License in the LICENSE.txt file, or at:

https://www.gnu.org/licenses/gpl-3.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

