# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.

from PySide2 import QtWidgets, QtCore, QtGui


class ResultsModel(QtCore.QAbstractTableModel):

    DATA_ROLE = QtCore.Qt.UserRole + 1

    @property
    def result_items(self):
        return self._result_items

    def __init__(self, parent=None):
        super(ResultsModel, self).__init__(parent=parent)
        self._result_items = []
        self.columns = []

    def set_result_items(self, result_items_list):
        self.beginResetModel()
        #self.clear()

        self.columns = list(result_items_list[0].keys())
        self._result_items = result_items_list
        self.endResetModel()

    def rowCount(self, parent=QtCore.QModelIndex()):
        if parent.column() > 0:
            return 0

        return len(self.result_items)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.columns)

    def removeRows(self, position, rows=1, index=QtCore.QModelIndex()):
        self.beginRemoveRows(index, position, position + rows - 1)

        self._result_items.pop(position)

        self.endRemoveRows()
        return True

    def data(self, index, role=QtCore.Qt.DisplayRole):
        row = index.row()
        column = index.column()

        if not index.isValid():
            return None

        item = self.result_items[row]
        data = item[self.columns[column]]

        if (
                role == QtCore.Qt.DisplayRole and
                index.column() in self.get_array_columns_index()
        ):
            return "Double click for details"
        elif (
                role == QtCore.Qt.DisplayRole and
                index.column() in self.get_dict_columns_index()
        ):
            return "Double click for details"

        # style the rest
        if role == QtCore.Qt.DisplayRole:
            return data

        elif role == QtCore.Qt.EditRole:
            return data

        elif role == self.DATA_ROLE:
            return item

        return None

    def headerData(self, section, orientation, role):
        if (
                orientation == QtCore.Qt.Horizontal and
                role == QtCore.Qt.DisplayRole
        ):
            return self.columns[section].replace('_', ' ').capitalize()

        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        if role == QtCore.Qt.EditRole:
            if value:
                self.dataChanged.emit(index, index)
                return True
            return False
        else:
            return super(ResultsModel, self).setData(index, value, role)

    def flags(self, index):
        if index.column() in self.get_array_columns_index():
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled
        elif index.column() in self.get_dict_columns_index():
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled
        else:
            return QtCore.Qt.ItemIsEnabled

    def get_array_columns_index(self):
        idxs = []
        if self.result_items:
            for k,v in self.result_items[0].items():
                if isinstance(v,list):
                    idxs.append(self.columns.index(k))
        return idxs

    def get_dict_columns_index(self):
        idxs = []
        if self.result_items:
            for k,v in self.result_items[0].items():
                if isinstance(v,dict):
                    idxs.append(self.columns.index(k))
        return idxs

    def get_icon_column_index(self):
        if self.result_items:
            for k,v in self.result_items[0].items():
                if k=='logo_url':
                    return self.columns.index(k)




class FilterProxyModel(QtCore.QSortFilterProxyModel):

    DATA_ROLE = ResultsModel.DATA_ROLE

    @property
    def result_items(self):
        return self.sourceModel().result_items

    def __init__(self, parent=None):
        super(FilterProxyModel, self).__init__(parent=parent)

        self.setDynamicSortFilter(True)
        self.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.setFilterKeyColumn(-1)

    def filterAcceptsRowItself(self, source_row, source_parent):
        return super(FilterProxyModel, self).filterAcceptsRow(
            source_row, source_parent
        )

    def filterAcceptsRow(self, source_row, source_parent):
        if self.filterAcceptsRowItself(source_row, source_parent):
            return True

        parent = source_parent
        while parent.isValid():
            if self.filterAcceptsRowItself(parent.row(), parent.parent()):
                return True
            parent = parent.parent()

        return False

    def lessThan(self, left, right):
        left_data = self.sourceModel().item(left)
        right_data = self.sourceModel().item(right)
        print((left_data, right_data))
        return left_data.id > right_data.id
