# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.

from PySide2 import QtWidgets

from covalent_python_wallet.ui.results_widget.model.results_widget import (
    ResultsModel, FilterProxyModel
)
from covalent_python_wallet.ui.results_widget.delegate.results_widget import (
    DetailsDelegate, ImageDelegate
)


class ResultsWidget(QtWidgets.QWidget):

    @property
    def result_items(self):
        return self._result_items

    def __init__(self, parent=None):
        super(ResultsWidget, self).__init__(parent)

        self._result_items = []

        self.pre_build()

    def pre_build(self):
        main_layout = QtWidgets.QVBoxLayout(self)
        self.setLayout(main_layout)

    def build(self):
        filter_layout = QtWidgets.QHBoxLayout()
        filter_label = QtWidgets.QLabel('Filter')
        self.filter_field = QtWidgets.QLineEdit()
        filter_layout.addWidget(filter_label)
        filter_layout.addWidget(self.filter_field)
        self.layout().addLayout(filter_layout)

        self.results_table_view = ResultsTableView(parent=self)
        self.layout().addWidget(self.results_table_view)

    def post_build(self):
        self.filter_field.textChanged.connect(self.on_search)


    def clear_items(self, layout=None):
        layout = layout or self.layout()
        self._result_items = []
        while layout.count() > 0:
            item = layout.takeAt(0)
            if not item:
                continue
            w = item.widget()
            if w:
                w.deleteLater()
            else:
                self.clear_items(item.layout())

    def set_error(self, method, result):
        error_label = QtWidgets.QLabel('Error runing method: {}'.format(method))
        error_result = QtWidgets.QTextEdit()

        error_result.setText(result)

        self._result_items.append(error_label)
        self._result_items.append(error_result)
        self.layout().addWidget(error_label)
        self.layout().addWidget(error_result)

    # def get_item(self, item):
    #     name = item.layout().itemAt(0).widget().text()
    #     if name.endswith('*'):
    #         name = name.replace('*','')
    #     value = item.layout().itemAt(1).widget().text()
    #     return name, value

    def on_search(self):
        value = self.filter_field.text()
        self.results_table_view.model().setFilterWildcard(value)

    def set_result_items(self, result_items_list):
        self.build()
        self.post_build()
        self._result_items = result_items_list
        self.results_table_view.set_result_items(self.result_items)



class ResultsTableView(QtWidgets.QTableView):

    def __init__(self, parent=None):
        super(ResultsTableView, self).__init__(parent=parent)
        self.pre_build()
        self.build()
        self.post_build()

    def pre_build(self):
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()

        self.setSelectionBehavior(
            QtWidgets.QAbstractItemView.SelectRows
        )

        self.horizontalHeader().setStretchLastSection(True)

    def build(self):
        self.results_model = ResultsModel(parent=self)
        self.proxy_model = FilterProxyModel(parent=self)

        self.proxy_model.setSourceModel(self.results_model)

        self.setModel(self.proxy_model)

        self.image_delegate = ImageDelegate(self)


    def post_build(self):
        pass

    def set_result_items(self, result_items_list):
        self.result_items = result_items_list
        self.results_model.set_result_items(self.result_items)

        if isinstance(self.result_items[0], dict):
            i = 0
            for v in list(self.result_items[0].values()):
                if isinstance(v, list):
                    details_delegate = DetailsDelegate(self)
                    self.setItemDelegateForColumn(
                        i, details_delegate
                    )
                if isinstance(v, dict):
                    dict_details_delegate = DetailsDelegate(self)
                    self.setItemDelegateForColumn(
                        i, dict_details_delegate
                    )
                i += 1

            if 'logo_url' in (self.result_items[0].keys()):
                self.setItemDelegateForColumn(
                    self.results_model.get_icon_column_index(), self.image_delegate
                )
        self.verticalHeader().setDefaultSectionSize(100)