# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.

import requests
from PySide2 import QtWidgets, QtCore, QtGui

class ImageDelegate(QtWidgets.QStyledItemDelegate):

    def __init__(self, parent=None):
        super(ImageDelegate, self).__init__(parent=parent)

    def paint(self, painter, option, index):

        path = index.model().data(index, QtCore.Qt.EditRole)
        request = requests.get(path,stream=True)
        assert request.status_code == 200
        image = QtGui.QImage()
        image.loadFromData(request.content)
        pixmap = QtGui.QPixmap.fromImage(image)
        pixmap.scaled(100,100, QtCore.Qt.KeepAspectRatio)
        painter.drawPixmap(option.rect, pixmap)
