# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.

from collections import OrderedDict

from PySide2 import QtWidgets

from covalent_python_wallet.ui.details_widget.model.details_widget import (
    DetailsModel, FilterProxyModel
)
from covalent_python_wallet.ui.details_widget.delegate.details_widget import (
    ImageDelegate
)

class DetailsWidget(QtWidgets.QWidget):

    @property
    def result_items(self):
        return self._result_items

    def __init__(self, parent=None):
        super(DetailsWidget, self).__init__(parent)

        self._result_items = []

        self.pre_build()
        self.setMinimumSize(500, 500)

    def pre_build(self):
        main_layout = QtWidgets.QVBoxLayout(self)
        self.setLayout(main_layout)

    def build(self):
        filter_layout = QtWidgets.QHBoxLayout()
        filter_label = QtWidgets.QLabel('Filter')
        self.filter_field = QtWidgets.QLineEdit()
        filter_layout.addWidget(filter_label)
        filter_layout.addWidget(self.filter_field)
        self.layout().addLayout(filter_layout)

        self.results_table_view = DetailsTableView(parent=self)
        self.layout().addWidget(self.results_table_view)

    def post_build(self):
        self.filter_field.textChanged.connect(self.on_search)

    def clear_items(self, layout=None):
        layout = layout or self.layout()
        self._result_items = []
        while layout.count() > 0:
            item = layout.takeAt(0)
            if not item:
                continue
            w = item.widget()
            if w:
                w.deleteLater()
            else:
                self.clear_items(item.layout())

    def set_error(self, method, result):
        error_label = QtWidgets.QLabel('Error runing method: {}'.format(method))
        error_result = QtWidgets.QTextEdit()

        error_result.setText(result)

        self._result_items.append(error_label)
        self._result_items.append(error_result)
        self.layout().addWidget(error_label)
        self.layout().addWidget(error_result)

    def on_search(self):
        value = self.filter_field.text()
        self.results_table_view.model().setFilterWildcard(value)

    def set_result_items(self, result_items_list):
        self.build()
        self.post_build()
        self._result_items = result_items_list
        self.results_table_view.set_result_items(self.result_items)

class DetailsTableView(QtWidgets.QTableView):

    def __init__(self, parent=None):
        super(DetailsTableView, self).__init__(parent=parent)
        self.pre_build()
        self.build()
        self.post_build()

    def pre_build(self):
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()

        self.setSelectionBehavior(
            QtWidgets.QAbstractItemView.SelectRows
        )

        self.horizontalHeader().setStretchLastSection(True)

        self.image_delegate = ImageDelegate(self)

    def build(self):
        self.results_model = DetailsModel(parent=self)
        self.proxy_model = FilterProxyModel(parent=self)

        self.proxy_model.setSourceModel(self.results_model)

        self.setModel(self.proxy_model)

    def post_build(self):
        pass

    def set_result_items(self, result_items_list):

        if not isinstance(result_items_list, list):
            result_items_list = [result_items_list]

        new_list = []
        if result_items_list:
            if isinstance(result_items_list[0], dict):
                if 'logo_url' not in list(result_items_list[0].keys()):
                    new_list = result_items_list
                else:
                    for item in result_items_list:
                        d = OrderedDict()
                        d['logo_url'] = item.get('logo_url')
                        for k, v in list(item.items()):
                            if k == 'logo_url':
                                continue
                            d[k] = v
                        new_list.append(d)
            else:
                new_list = result_items_list

        self.result_items = new_list
        self.results_model.set_result_items(self.result_items)

        if isinstance(self.result_items[0], dict):
            if 'logo_url' in (self.result_items[0].keys()):
                self.setItemDelegateForColumn(
                    self.results_model.get_icon_column_index(), self.image_delegate
                )
        self.verticalHeader().setDefaultSectionSize(100)
