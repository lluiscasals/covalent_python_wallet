# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.

from collections import OrderedDict
from PySide2 import QtWidgets

from covalent_api import url_utils as utils

from covalent_python_wallet.ui.parameters_widget import ParametersWidget
from covalent_python_wallet.ui.results_widget import ResultsWidget

class ClassWidget(QtWidgets.QWidget):

    @property
    def class_type(self):
        return self._class_type

    @property
    def class_methods(self):
        return self._class_methods

    def __init__(self, session, class_type, parent=None):
        super(ClassWidget, self).__init__(parent)

        not_initialized_class = class_type
        self._class_type = class_type(session)
        self._class_methods = utils.get_class_methods(not_initialized_class)
        self.current_meth = None

        self.pre_build()
        self.build()
        self.post_build()

    def pre_build(self):
        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

    def build(self):
        self.query_selector = QtWidgets.QComboBox()
        self.query_selector.addItems(self.class_methods)

        self.parameters_widget = ParametersWidget()

        self.result_widget = ResultsWidget()

        self.run_button = QtWidgets.QPushButton('Query')

        self.layout().addWidget(self.query_selector)
        self.layout().addWidget(self.parameters_widget)
        self.layout().addWidget(self.result_widget)
        self.layout().addWidget(self.run_button)


    def post_build(self):
        self.query_selector.currentTextChanged.connect(self._on_change_query)
        self.run_button.clicked.connect(self._on_run_query)
        self._on_change_query(self.query_selector.currentText())

    def _on_change_query(self, meth_name):
        self.parameters_widget.clear_items()
        self.result_widget.clear_items()
        self.current_meth = getattr(self.class_type, meth_name)
        required_args, optional_args = utils.get_method_arguments(self.current_meth)
        for arg in required_args:
            self.parameters_widget.add_item(arg, required=True)
        for arg in optional_args:
            self.parameters_widget.add_item(arg, required=False)

    def _on_run_query(self):
        parameters = {}
        for item in self.parameters_widget.items:
            arg_name, arg_value = self.parameters_widget.get_item(item)
            if arg_value:
                parameters[arg_name] = arg_value
        self.run_query(self.current_meth, parameters)

    def run_query(self, method, parameters):
        try:
            result = method(**parameters)
        except Exception as e:
            self.show_error(method, e)
            raise Exception(
                "Could not execute the method {}. \n Parameters: {} \n Error: {}".format(
                    method, parameters, e
                )
            )

        is_valid = self.validate_result(result)
        if not is_valid:
            self.show_error(method, result)
            return

        self.show_result(result)

    def validate_result(self, result):
        if not result:
            return False
        if isinstance(result, dict):
            if result.get('error'):
                return False
        else:
            return False
        return True

    def show_result(self, result):
        self.result_widget.clear_items()
        items = result.get('items')
        if not items:
            items = result.get('data').get('items')
        if not items:
            items = [result.get('data')]
        if not items:
            self.show_error('Result not implemented', result)
        if items:
            new_list = []
            if 'logo_url' not in list(items[0].keys()):
                new_list = items
            else:
                for item in items:
                    d = OrderedDict()
                    d['logo_url'] = item.get('logo_url')
                    for k, v in list(item.items()):
                        if k == 'logo_url':
                            continue
                        d[k] = v
                    new_list.append(d)
            self.result_widget.set_result_items(new_list)

    def show_error(self, method, result):
        self.result_widget.clear_items()
        self.result_widget.set_error(method, str(result))