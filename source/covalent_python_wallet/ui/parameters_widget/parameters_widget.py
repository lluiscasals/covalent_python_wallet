# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.

from PySide2 import QtWidgets, QtCore, QtGui

class ParametersWidget(QtWidgets.QGroupBox):

    @property
    def items(self):
        return self._items

    def __init__(self, parent=None):
        super(ParametersWidget, self).__init__(parent)

        self._items = []

        self.pre_build()

    def pre_build(self):
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setAlignment(QtCore.Qt.AlignTop)

        self.setLayout(layout)


    def clear_items(self):
        self._items = []
        while self.layout().count() > 0:
            item = self.layout().takeAt(0)
            if not item:
                continue
            w = item.widget()
            if w:
                w.deleteLater()

    def add_item(self, param_name, required=False):
        item = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout()
        placeholder_text = None
        tool_tip_text = None
        if param_name == 'chain_id':
            placeholder_text = '43114 for Avalanche C-Chain Mainnet'
            tool_tip_text = (
                '1 for Ethereum Mainnet,\n137 for Polygon/Matic Mainnet,\n80001 '
                'for Polygon/Matic Mumbai Testnet,\n56 for Binance Smart Chain,\n'
                '43114 for Avalanche C-Chain Mainnet,\n43113 for Fuji C-Chain '
                'Testnet,\n250 for Fantom Opera Mainnet.'
            )
        if required:
            param_name = param_name+'*'

        name = QtWidgets.QLabel(param_name)
        value = QtWidgets.QLineEdit()
        if placeholder_text:
            value.setPlaceholderText(placeholder_text)
        if tool_tip_text:
            value.setToolTip(tool_tip_text)

        layout.addWidget(name)
        layout.addWidget(value)

        item.setLayout(layout)

        self._items.append(item)
        self.layout().addWidget(item)

    def get_item(self, item):
        name = item.layout().itemAt(0).widget().text()
        if name.endswith('*'):
            name = name.replace('*','')
        value = item.layout().itemAt(1).widget().text()
        return name, value