# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.
from covalent_python_wallet.ui.parameters_widget.parameters_widget import ParametersWidget