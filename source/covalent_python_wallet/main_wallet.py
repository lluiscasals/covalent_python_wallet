# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.

import sys
import os

from PySide2 import QtCore, QtWidgets

import covalent_api
from covalent_python_wallet.ui.class_widget import ClassWidget


class QtMainWallet(QtWidgets.QMainWindow):

    @property
    def session(self):
        return self._session

    @property
    def api_classes(self):
        return self._api_classes

    def __init__(self, session, parent=None):
        super(QtMainWallet, self).__init__(parent)

        self._session = session
        self._api_classes = {
            'classA': covalent_api.ClassA,
            'ClassB': covalent_api.ClassB,
            'Pricing': covalent_api.Pricing
        }

        self.pre_build()
        self.build()
        self.post_build()

    def pre_build(self):
        pass


    def build(self):
        self.tab_widget = QtWidgets.QTabWidget()

        for k, v in self.api_classes.items():
            class_widget = ClassWidget(self.session, v)
            self.tab_widget.addTab(class_widget, k)

        self.setCentralWidget(self.tab_widget)
    def post_build(self):
        pass


def main():

    app = QtWidgets.QApplication(sys.argv)

    session = covalent_api.Session(
        server_url='https://api.covalenthq.com',
        api_key=os.environ.get('COVALENT_API_KEY')
    )

    client = QtMainWallet(session)
    client.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()