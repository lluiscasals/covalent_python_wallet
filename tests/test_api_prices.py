# :coding: utf-8
# :copyright: Copyright (c) 2021 Lluis Casals Marsol
# Please see the LICENSE file that should have been included as part of this
# package.


import covalent_api

import sys
import os
from PySide2 import QtWidgets

app = QtWidgets.QApplication(sys.argv)

session = covalent_api.Session(
    server_url='https://api.covalenthq.com',
    api_key=os.environ.get('COVALENT_API_KEY')
)

from covalent_python_wallet import main_wallet
client = main_wallet.QtMainWallet(session)
client.show()
sys.exit(app.exec_())